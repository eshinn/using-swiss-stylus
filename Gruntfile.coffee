module.exports = (grunt) ->

  # Configuration
  grunt.initConfig
    pkg: require './package.json'

  # Load tasks from grunt-tasks folder
  grunt.loadTasks 'grunt-tasks'

  # Register task aliases
  grunt.registerTask 'default', [
    'clean:all'
    'html:dev'
    'css:dev'
    # 'js:dev'
    'connect'
    'browserSync'
    'watch'
  ]

  # NOTE:
  # - Initial Grunt call runs clean:all
  # - WATCH will clean individual types
  # - WATCH COPY will be done by individual type-tasks, not globally

  # HTML
  grunt.registerTask 'html:dev', [
    'jade:dev'
  ]

  # CSS
  grunt.registerTask 'css:dev', [
    'stylus:dev'
    'postcss'
  ]

  # JS
  # grunt.registerTask 'js:dev', [
  #   'copy:js'
  # ]
